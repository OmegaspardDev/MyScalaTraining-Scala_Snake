package core

import Constant_Utils.DEFAULT_ARRAY_SIZE

/**
  * Created by Gaspard.Diop-Dubois on 22/09/2017.
  */
/**
  * This class represent the board where the snake can move.
  * This is two dimensional array.
  * If the snake reach a side, he will be transported to the opposite side of the contacted side.
  */
class Board {
  private val board = Array(50)[Array[Cell]]

}

class Cell {}

