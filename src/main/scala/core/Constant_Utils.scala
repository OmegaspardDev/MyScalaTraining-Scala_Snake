package core

/**
  * Created by Gaspard.Diop-Dubois on 22/09/2017.
  */
object Constant_Utils {
  final val DEFAULT_ARRAY_SIZE = 50
  final val DEFAULT_CELL_SIZE = 10
}
