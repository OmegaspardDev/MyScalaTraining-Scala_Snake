package core

import java.util

import Constant_Utils.{DEFAULT_ARRAY_SIZE, DEFAULT_CELL_SIZE}
import core.Shifting.Shifting
import core.BodyPart.shiftBodyPart

import scala.collection.mutable.ListBuffer

/**
  * Created by Gaspard.Diop-Dubois on 22/09/2017.
  */
/**
  * This class represent the snake, the first idea is to represent a snake by an array.
  * Each cell of the array represent the body part of the snake.
  * In each cell, we store the position of the body part on board.
  */
class Snake(headArg:BodyPart, tailArg:BodyPart) {
  require(headArg != tailArg)
  private var head = headArg
  private var tail = tailArg
  private var body = Array(head, tail)
  private var direction = Shifting.Up

  def makeShifting(shifting:Shifting):Unit = {
    //Faire une méthode qui prend en arguement un déplacement
    //Ou bien faire un pattern mathcing qui prend l'info du déplacement et qui l'applique au snake.
    val newHead = shiftBodyPart(shifting, head)
    body =Array(newHead) ++ body.slice(1, body.length)
  }

  def eat(eatable:Eatable): Unit = {
    def generateNewBodyPart(tail:BodyPart, beforeTail:BodyPart): BodyPart = {
      val shiftingVector = tail.getDirection(beforeTail)
      new BodyPart(tail.x + shiftingVector._1, tail.y + shiftingVector._2)
    }
    body = body ++ Array(generateNewBodyPart(tail, body(body.length -1)))
  }

}

object BodyPart {
  private val shiftingMap = Map(Shifting.Up -> {(part:BodyPart) => BodyPart(part.x, part.y - 1)},
    Shifting.Down -> {(part:BodyPart) => BodyPart(part.x, part.y + 1)},
    Shifting.Left -> {(part:BodyPart) => BodyPart(part.x - 1, part.y)},
    Shifting.Right -> {(part:BodyPart) => BodyPart(part.x +1, part.y)})

  def apply(xArg: Int, yArg: Int): BodyPart = new BodyPart(xArg, yArg)
  def shiftBodyPart(shifting:Shifting, part:BodyPart) = shiftingMap(shifting)(part)
}

class BodyPart(xArg:Int, yArg:Int) {
  require(xArg >= 0, yArg >= 0)
  val x = xArg
  val y = yArg
  var listEffect = new ListBuffer()//Une liste d'effet, c'est des lambdas qui seront appliqués.

  def getDirection(part:BodyPart):(Int,Int) = {
    if(this.x == part.x) {
      val result = this.y - part.y

      if( result > 0) return (0,-1) else return (0,1)
    }
    val result = this.x -part.x
    if(result > 0) return (-1, 0) else return (1, 0)
  }
}

object Shifting extends Enumeration{
  type Shifting = Value
  val Up, Down, Left, Right = Value
}